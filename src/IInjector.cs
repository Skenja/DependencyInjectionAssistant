﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace DependencyInjectionAssistant
{
	public interface IInjector
	{
		void InjectTransient(IServiceCollection services, Type contract, Type implementation);
		void InjectScoped(IServiceCollection services, Type contract, Type implementation);
		void InjectSingleton(IServiceCollection services, Type contract, Type implementation);
	}
}