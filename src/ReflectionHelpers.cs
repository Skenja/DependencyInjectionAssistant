﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DependencyInjectionAssistant
{
	public static class ReflectionHelper
	{
		public static List<Type> GetTypes(this IEnumerable<Assembly> assemblies)
		{
			return assemblies
				.GetLoadableTypes()
				.ToList();
		}

		public static List<Type> GetTypesImplementing<TBaseType>(this IEnumerable<Assembly> assemblies)
		{
			return assemblies
				.GetLoadableTypes()
				.GetTypesImplementing<TBaseType>();
		}

		public static List<Type> GetLoadableTypes(this IEnumerable<Assembly> assemblies)
		{
			try
			{
				return assemblies
					.SelectMany(a => a.GetTypes())
					.ToList();
			}
			catch (ReflectionTypeLoadException e)
			{
				return e.Types
					.Where(t => t != null)
					.ToList();
			}
		}

		public static List<Type> GetTypesImplementing<TBaseType>(this IEnumerable<Type> types)
		{
			var baseType = typeof(TBaseType);

			return types
				.Where(t => baseType.IsAssignableFrom(t))
				.Where(t => !t.FullName.Equals(baseType.FullName))
				.ToList();
		}

		public static T GetCustomAttribute<T>(this Type type)
		{
			return type.GetTypeInfo()
				.CustomAttributes
				.Where(a => typeof(T).IsAssignableFrom(a.AttributeType))
				.Select(a => (T) Activator.CreateInstance(a.AttributeType))
				.FirstOrDefault();
		}

		public static bool HasCustomAttribute<T>(this Type type)
		{
			return type.GetTypeInfo()
				.CustomAttributes
				.FirstOrDefault(a => typeof(T).IsAssignableFrom(a.AttributeType)) != null;
		}

		public static List<Type> GetInterfaces(this List<Type> list)
		{
			return list
				.Where(t => t.IsInterface)
				.ToList();
		}

		public static List<Type> GetClasses(this List<Type> list)
		{
			return list
				.Where(t => !t.IsInterface)
				.Where(t => t.IsClass)
				.ToList();
		}
	}
}