﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace DependencyInjectionAssistant
{
	public static class DependencyInjectionBinder
	{
		/// <summary>
		/// This extension method wil bind all your dependancies which in their inheritance tree have IInjectable interface.
		/// </summary>
		public static void BindInheritedDependancies(this IServiceCollection services, ImplementationEnvironment environment)
		{
			var namespaceIdentifier = GetDefaultNamespaceIdentifier();

			services.BindInheritedDependancies<DefaultInjector>(environment, namespaceIdentifier);
		}

		/// <summary>
		/// This extension method wil bind all your dependancies which in their inheritance tree have IInjectable interface.
		/// </summary>
		public static void BindInheritedDependancies<TInjector>(this IServiceCollection services, ImplementationEnvironment environment)
			where TInjector : IInjector
		{
			var namespaceIdentifier = GetDefaultNamespaceIdentifier();

			services.BindInheritedDependancies<TInjector>(environment, namespaceIdentifier);
		}

		/// <summary>
		/// This extension method wil bind all your dependancies which in their inheritance tree have IInjectable interface.
		/// </summary>
		public static void BindInheritedDependancies(this IServiceCollection services, ImplementationEnvironment environment, string namespaceIdentifier)
		{
			services.BindInheritedDependancies<DefaultInjector>(environment, namespaceIdentifier);
		}

		/// <summary>
		/// This extension method wil bind all your dependancies which in their inheritance tree have IInjectable interface.
		/// </summary>
		public static void BindInheritedDependancies<TInjector>(this IServiceCollection services, ImplementationEnvironment environment, string namespaceIdentifier)
			where TInjector : IInjector
		{
			var injector = Activator.CreateInstance<TInjector>();

			var allTypes = AppDomain
				.GetAssemblies(namespaceIdentifier)
				.GetTypesImplementing<IInjectable>();

			var allContracts = allTypes
				.GetInterfaces()
				.ToList();

			foreach (var contract in allContracts)
			{
				BindContractsWithImplementations(services, contract, allTypes, environment, injector);
			}
		}

		/// <summary>
		/// This method will bind all your dependancies whose implementations have one of the attributes([Transient], [Scoped], [Singleton]).
		/// </summary>
		public static void BindImplementedDependancies(this IServiceCollection services, ImplementationEnvironment environment)
		{
			var namespaceIdentifier = GetDefaultNamespaceIdentifier();

			services.BindImplementedDependancies<DefaultInjector>(environment, namespaceIdentifier);
		}

		/// <summary>
		/// This method will bind all your dependancies whose implementations have one of the attributes([Transient], [Scoped], [Singleton]).
		/// </summary>
		public static void BindImplementedDependancies<TInjector>(this IServiceCollection services, ImplementationEnvironment environment)
			where TInjector : IInjector
		{
			var namespaceIdentifier = GetDefaultNamespaceIdentifier();

			services.BindImplementedDependancies<TInjector>(environment, namespaceIdentifier);
		}

		/// <summary>
		/// This method will bind all your dependancies whose implementations have one of the attributes([Transient], [Scoped], [Singleton]).
		/// </summary>
		public static void BindImplementedDependancies(this IServiceCollection services, ImplementationEnvironment environment, string namespaceIdentifier)
		{
			services.BindImplementedDependancies<DefaultInjector>(environment, namespaceIdentifier);
		}

		/// <summary>
		/// This method will bind all your dependancies whose implementations have one of the attributes([Transient], [Scoped], [Singleton]).
		/// </summary>
		public static void BindImplementedDependancies<TInjector>(this IServiceCollection services, ImplementationEnvironment environment, string namespaceIdentifier)
			where TInjector : IInjector
		{
			var injector = Activator.CreateInstance<TInjector>();

			var allTypes = AppDomain
				.GetAssemblies(namespaceIdentifier)
				.GetTypes();

			var allBindableImplementations = allTypes
				.GetClasses()
				.Where(t => t.HasCustomAttribute<ObjectLifetimeAttribute>())
				.ToList();

			var allContracts = allTypes
				.GetInterfaces()
				.Where(it => allBindableImplementations
					.Any(t => it.IsAssignableFrom(t)))
				.ToList();

			foreach (var contract in allContracts)
			{
				BindContractsWithImplementations(services, contract, allBindableImplementations, environment, injector);
			}
		}

		private static void BindContractsWithImplementations(
			IServiceCollection services,
			Type contract,
			List<Type> implementations,
			ImplementationEnvironment environment,
			IInjector injector)
		{
			var filteredImplementations = implementations
				.GetClasses()
				.Where(t => contract.IsAssignableFrom(t))
				.Select(t => new { Implementation = t, Attribute = t.GetCustomAttribute<ObjectLifetimeAttribute>() })
				.Where(pair => pair.Attribute != null)
				.Where(pair => pair.Attribute.ShouldBeBound(environment))
				.ToList();

			if (filteredImplementations.Count > 1)
			{
				var implementationsNamesFlattened = string.Join(", ", filteredImplementations.Select(i => i.Implementation.FullName));

				throw new ApplicationException($"{contract.Name} has too many valid implementations for chosen environment ({implementationsNamesFlattened}).");
			}

			var implementation = filteredImplementations.First();

			if (implementation.Attribute.IsTransient)
			{
				injector.InjectTransient(services, contract, implementation.Implementation);
			}
			else if (implementation.Attribute.IsScoped)
			{
				injector.InjectScoped(services, contract, implementation.Implementation);
			}
			else if (implementation.Attribute.IsSingleton)
			{
				injector.InjectSingleton(services, contract, implementation.Implementation);
			}
		}

		#region Helpers

		private static string GetDefaultNamespaceIdentifier()
		{
			return Assembly
				.GetEntryAssembly()
				.FullName
				.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries)
				.FirstOrDefault();
		}

		#endregion Helpers
	}
}