﻿using System;

namespace DependencyInjectionAssistant
{
	public class ObjectLifetimeAttribute : Attribute
	{
		protected ObjectLifetime Lifetime { get; }
		public ImplementationEnvironment Environment { get; protected set; }

		protected ObjectLifetimeAttribute(ObjectLifetime lifetime, ImplementationEnvironment environment)
		{
			this.Lifetime = lifetime;
			this.Environment = environment;
		}

		public bool IsTransient => this.Lifetime == ObjectLifetime.Transient;
		public bool IsScoped => this.Lifetime == ObjectLifetime.Scoped;
		public bool IsSingleton => this.Lifetime == ObjectLifetime.Singleton;

		public bool ShouldBeBound(ImplementationEnvironment environment)
		{
			return this.Environment.HasFlag(environment);
		}

		protected enum ObjectLifetime
		{
			Transient = 1,
			Scoped = 2,
			Singleton = 3
		}
	}

	[Flags]
	public enum ImplementationEnvironment
	{
		Production = 1,
		Test = 2,
	}

	public class TransientAttribute : ObjectLifetimeAttribute
	{
		public TransientAttribute(ImplementationEnvironment environment = ImplementationEnvironment.Production | ImplementationEnvironment.Test)
			: base(ObjectLifetime.Transient, environment)
		{
		}
	}

	public class ScopedAttribute : ObjectLifetimeAttribute
	{
		public ScopedAttribute(ImplementationEnvironment environment = ImplementationEnvironment.Production | ImplementationEnvironment.Test)
			: base(ObjectLifetime.Scoped, environment)
		{
		}
	}

	public class SingletonAttribute : ObjectLifetimeAttribute
	{
		public SingletonAttribute(ImplementationEnvironment environment = ImplementationEnvironment.Production | ImplementationEnvironment.Test)
			: base(ObjectLifetime.Singleton, environment)
		{
		}
	}
}